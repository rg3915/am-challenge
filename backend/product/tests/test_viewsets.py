import json

from django.test import TestCase
from rest_framework import status

from backend.product.models import Product


class ViewsetTest(TestCase):

    def setUp(self) -> None:
        self.product = Product.objects.create(title='caneta', price=1.5)

    def test_product_viewsets(self):
        response = self.client.get(
            '/api/product/products/',
            content_type='application/json'
        )
        resultado = json.loads(response.content)
        esperado = [
            {
                "id": 1,
                "title": "caneta",
                "price": "1.50",
                "comission": "0.00"
            }
        ]

        self.assertEqual(esperado, resultado)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_product_viewsets_detail(self):
        response = self.client.get(
            '/api/product/products/1/',
            content_type='application/json'
        )
        resultado = json.loads(response.content)
        esperado = {
            "id": 1,
            "title": "caneta",
            "price": "1.50",
            "comission": "0.00"
        }

        self.assertEqual(esperado, resultado)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
