from rest_framework import viewsets

from .models import Product
from .serializers import ProductSerializer


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

    def get_queryset(self):
        # Aplica um filtro entre duas datas.
        queryset = super(ProductViewSet, self).get_queryset()
        search = self.request.query_params.get('search')

        if search:
            if search.isnumeric():
                queryset = queryset.filter(pk=search)
            else:
                queryset = queryset.filter(title__icontains=search)
        return queryset
