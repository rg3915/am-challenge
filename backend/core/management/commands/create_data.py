from django.core.management.base import BaseCommand

from backend.crm.models import Customer, Seller
from backend.product.models import Product

products = [
    {
        'title': 'apontador',
        'price': 1.5,
        'comission': 0.04,
    },
    {
        'title': 'borracha',
        'price': 2.5,
        'comission': 0.06,
    },
    {
        'title': 'caneta',
        'price': 1.4,
        'comission': 0.01,
    },
    {
        'title': 'papel sulfite',
        'price': 6.5,
        'comission': 0.09,
    },
]


class Command(BaseCommand):
    help = 'Cria alguns dados iniciais.'

    def handle(self, *args, **options):
        self.stdout.write('Criando os clientes...')
        Customer.objects.create(name='Acme')
        Customer.objects.create(name='Looney')

        self.stdout.write('Criando os vendedores...')
        Seller.objects.create(name='Abel')
        Seller.objects.create(name='Carla')
        Seller.objects.create(name='Regis')

        self.stdout.write('Criando os produtos...')

        product_list = []
        for product in products:
            product_obj = Product(
                title=product['title'],
                price=product['price'],
                comission=product['comission'],
            )
            product_list.append(product_obj)
        Product.objects.bulk_create(product_list)
