from django.test import TestCase
from faker import Faker

from backend.crm.models import Customer, Seller
from backend.product.models import Product
from backend.sale.models import Sale, SaleItems
from backend.sale.serializers import SaleItemsSerializer, SaleSerializer


class TestSerializers(TestCase):

    def setUp(self) -> None:
        self.faker = Faker()
        self.customer = Customer.objects.create(name=self.faker.name())
        self.seller = Seller.objects.create(name=self.faker.name())

        self.sale = Sale.objects.create(
            customer=self.customer,
            seller=self.seller
        )
        self.product1 = Product.objects.create(
            title='Caneta',
            price=1.5,
            comission=0.01
        )
        self.product2 = Product.objects.create(
            title='Borracha',
            price=0.5,
            comission=0.05
        )
        self.sale_items = SaleItems.objects.create(
            sale=self.sale,
            product=self.product1,
            quantity=3,
            price_sale=self.product1.price,
        )
        self.sale_serializer = SaleSerializer(instance=self.sale)
        self.sale_items_serializer = SaleItemsSerializer(
            instance=self.sale_items)

    def test_sale_contem_campos_esperados(self):
        data = self.sale_serializer.data
        esperado = set([
            'customer',
            'seller',
            'get_total_comission',
        ])
        resultado = set(data.keys())

        self.assertEqual(esperado, resultado)

    def test_sale_items_contem_campos_esperados(self):
        data = self.sale_items_serializer.data
        esperado = set([
            'sale',
            'product',
            'quantity',
            'price_sale',
            'comission',
        ])
        resultado = set(data.keys())

        self.assertEqual(esperado, resultado)
