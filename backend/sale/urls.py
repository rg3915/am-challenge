from django.urls import include, path
from rest_framework import routers

from backend.sale import views as v

app_name = 'sale'

router = routers.DefaultRouter()
router.register('sales', v.SaleViewSet)
router.register('saleitems', v.SaleItemsViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
