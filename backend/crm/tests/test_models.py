from django.test import TestCase

from backend.crm.models import Customer, Seller


class TestCustomer(TestCase):

    def setUp(self) -> None:
        self.customer = Customer.objects.create(name='Regis')

    def test_customer_deve_retornar_atributos(self):
        fields = ('name',)

        for field in fields:
            with self.subTest():
                self.assertTrue(hasattr(Customer, field))

    def test_customer_str_deve_retornar_nome(self):
        esperado = 'Regis'
        resultado = str(self.customer)
        self.assertEqual(esperado, resultado)


class TestSeller(TestCase):

    def setUp(self) -> None:
        self.seller = Seller.objects.create(name='Regis')

    def test_seller_deve_retornar_atributos(self):
        fields = ('name',)

        for field in fields:
            with self.subTest():
                self.assertTrue(hasattr(Seller, field))

    def test_seller_str_deve_retornar_nome(self):
        esperado = 'Regis'
        resultado = str(self.seller)
        self.assertEqual(esperado, resultado)
