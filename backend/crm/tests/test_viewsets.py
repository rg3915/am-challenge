import json

from django.test import TestCase
from rest_framework import status

from backend.crm.models import Customer, Seller


class UrlTest(TestCase):

    def setUp(self) -> None:
        self.customer = Customer.objects.create(name='Regis')
        self.seller = Seller.objects.create(name='Regis')

    def test_customer_viewsets(self):
        response = self.client.get(
            '/api/crm/customers/',
            content_type='application/json'
        )
        resultado = json.loads(response.content)
        esperado = [
            {
                "id": 1,
                "name": "Regis"
            }
        ]

        self.assertEqual(esperado, resultado)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_customer_viewsets_detail(self):
        response = self.client.get(
            '/api/crm/customers/1/',
            content_type='application/json'
        )
        resultado = json.loads(response.content)
        esperado = {
            "id": 1,
            "name": "Regis"
        }

        self.assertEqual(esperado, resultado)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_seller_viewsets(self):
        response = self.client.get(
            '/api/crm/sellers/',
            content_type='application/json'
        )
        resultado = json.loads(response.content)
        esperado = [
            {
                "id": 1,
                "name": "Regis"
            }
        ]

        self.assertEqual(esperado, resultado)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_seller_viewsets_detail(self):
        response = self.client.get(
            '/api/crm/sellers/1/',
            content_type='application/json'
        )
        resultado = json.loads(response.content)
        esperado = {
            "id": 1,
            "name": "Regis"
        }

        self.assertEqual(esperado, resultado)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
