from django.test import TestCase

from backend.crm.models import Customer, Seller
from backend.crm.serializers import CustomerSerializer, SellerSerializer


class UrlTest(TestCase):

    def setUp(self) -> None:
        self.customer = Customer.objects.create(name='Regis')
        self.seller = Seller.objects.create(name='Regis')
        self.customer_serializer = CustomerSerializer(instance=self.customer)
        self.seller_serializer = SellerSerializer(instance=self.seller)

    def test_customer_contem_campos_esperados(self):
        data = self.customer_serializer.data

        self.assertEqual(set(data.keys()), set(['id', 'name']))

    def test_seller_contem_campos_esperados(self):
        data = self.seller_serializer.data

        self.assertEqual(set(data.keys()), set(['id', 'name']))
