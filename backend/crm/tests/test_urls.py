from django.test import TestCase

from backend.crm.models import Customer, Seller


class UrlTest(TestCase):

    def setUp(self) -> None:
        self.customer = Customer.objects.create(name='Regis')
        self.seller = Seller.objects.create(name='Regis')

    def test_url_customers(self):
        url = '/api/crm/customers/'

        self.r = self.client.get(url)
        self.assertEqual(200, self.r.status_code)

    def test_url_customer_detail(self):
        url = '/api/crm/customers/1/'

        self.r = self.client.get(url)
        self.assertEqual(200, self.r.status_code)

    def test_url_sellers(self):
        url = '/api/crm/sellers/'

        self.r = self.client.get(url)
        self.assertEqual(200, self.r.status_code)

    def test_url_seller_detail(self):
        url = '/api/crm/sellers/1/'

        self.r = self.client.get(url)
        self.assertEqual(200, self.r.status_code)
