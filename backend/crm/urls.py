from django.urls import include, path
from rest_framework import routers

from backend.crm import views as v

app_name = 'crm'

router = routers.DefaultRouter()
router.register(r'customers', v.CustomerViewSet)
router.register(r'sellers', v.SellerViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
