# am-challenge

Teste para AMcom baseado em TDD + DRF + VueJS + Cypress + Docker

# Backend

## Parte 1. Contextualização:

A empresa XYZ vende produtos e serviços para seus clientes.

As vendas podem ou não ter vendedor e podem ter um ou mais itens.

Cada produto ou serviço (item de venda) quando vendido, pode gerar uma comissão entre `0` e `10%`, se a venda for entre `00:00` e `12:00` a comissão de cada item de venda não pode ser maior que `5%`, se a venda for entre `12:00:01` e `23:59:59` a comissão de cada item de venda deve ser no mínimo `4%`.

Entende-se item de venda por:

* (QTD) * produto ou serviço
* **Ex.:** 2 - CANETA - cada um custando `R$ 3,00` dá um total de `R$ 6,00`. A % de comissão desse item de venda é de `4%`, ou seja `R$ 0,24`.
* O total de comissão da venda é o total das somas das comissões dos itens da venda. **Ex.:** A venda tem três itens de venda, cada um gerou uma comissão de `R$ 0,10`. A comissão total da venda é `R$ 0,30`.

Desta forma:

1. Desenvolva um CRUD baseado nessas entidades que você percebeu.
2. O CRUD deve ser feito em Flask ou em Django com banco de dados preferencialmente Postgres.
3. Deve ser uma API rest com o uso correto dos verbos HTTP para criação, exclusão, etc.

As perguntas a serem respondidas são:

1. Dado um intervalo de tempo, quanto de comissão um vendedor tem direito?
2. Quais produtos e serviços um determinado cliente comprou num intervalo de tempo?
3. Quais os produtos e serviços mais vendidos num dado intervalo de datas? Listar em ordem
decrescente.

## Parte 2

O seu projeto deve estar acessível para download, em algum repositório git e deve conter instruções
detalhadas para executar no README do projeto.

### Essencial:

* Testes unitários e de integração (valorizamos muito isso, quanto mais melhor).
* Código limpo com padronização de código, escolha um padrão e mantenha-se nele (PyCQA, Flake8, Black, Pylint, Pyflakes, Pep8, etc).
* Boa documentação explicando como executar o projeto.

### Pontos extras:

* Aplicação de 12 factor: https://12factor.net/pt_br/
* Diagrama de entidade de relacionamento das entidades que você percebeu.
* CI/CD.
* Docker.
* Exemplo no postman.


# Frontend

## Parte 1. Contextualização:

Com a API pronta, faremos um ponto de vendas!
O nosso UX já preparou o protótipo e também as orientações no figma para facilitar seu trabalho.
https://zpl.io/bLnMwGQ

![mockup.png](img/mockup.png)

Mensagens que deverão ser cadastradas:

![message_error.png](img/message_error.png)

![message_success.png](img/message_success.png)

A imagem acima é para exemplo. A intenção é somente realizar vendas dos produtos e serviços da empresa xyz. Você pode fazer uma carga de dados API que você criou.

Obs: um item de venda é:

* (QTD) * produto ou serviço = total

Este ponto de vendas deve ter:

1. Busca de produto/serviço.
2. Seleção de quantidade (podendo ser fracionado).
3. Lista de itens da venda.
4. Exclusão dos itens da venda.
5. Seleção de um vendedor.
6. Seleção de um cliente.
7. Mostrar o total da venda conforme os itens de venda vão sendo adicionados.
8. Botão cancelar para limpar a tela e não efetuar a venda.
9. Botão confirmar para mandar a requisição para a API e confirmar a venda.
10. Deve aparecer um popup avisando que a venda foi bem sucedida ou não.

## Parte 2

O seu projeto deve estar acessível para download, em algum repositório git e deve conter instruções detalhadas para executar no README do projeto.

### Essencial:

* Código limpo com padronização de código, escolha um padrão e mantenha-se nele (Eslint, Airbnb,
Prettier, etc.).
* Boa documentação explicando como executar o projeto.

### Pontos extras:

* Testes unitários e de integração (valorizamos muito isso, quanto mais melhor).
* Atalhos de teclado. Ex.: F8 para buscar item, F10 para confirmar a venda.
* Preferencialmente ReactJS.
* Boa responsividade.
* Testes end-to-end (Selenium, Cypress, etc.).
