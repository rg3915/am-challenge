# am-challenge

Teste para AMcom baseado em TDD + DRF + VueJS + Cypress + Docker

Leia os requisitos do projeto em [desafio.md](desafio.md)

## Este projeto foi feito com:

* [Python 3.8.2](https://www.python.org/)
* [Django 2.2.15](https://www.djangoproject.com/)
* [Django Rest Framework 3.11.1](https://www.django-rest-framework.org/)
* [VueJS 2.6.11](https://vuejs.org/)
* [axios 0.20.0](https://github.com/axios/axios)
* [bootstrap-vue 2.16.0](https://bootstrap-vue.org)
* [trevoreyre/autocomplete-vue 2.2.0](https://github.com/trevoreyre/autocomplete)
* [Cypress](https://www.cypress.io/)
* [sweetalert2](https://sweetalert2.github.io/)
* [vue-toast-notification](https://github.com/ankurk91/vue-toast-notification)

## Como rodar o projeto?

```
git clone https://gitlab.com/rg3915/am-challenge.git
cd am-challenge
```

### Sem Docker faça os passos a seguir

* Crie um virtualenv com Python 3.
* Ative o virtualenv.
* Instale as dependências.
* Rode as migrações.
* Rode os testes.
* Crie um superusuário.

```
# Rodando o backend
python3 -m venv .venv
source .venv/bin/activate
pip install -U pip && pip install -r requirements.txt
python contrib/env_gen.py
echo -e "\nDOCKER=False" >> .env
python manage.py migrate
python manage.py test
python manage.py create_data
python manage.py createsuperuser --username='admin' --email=''
```

### Rodando o frontend

Numa outra aba do terminal faça

```
cd frontend
npm install
npm run serve
```


### Rodando com Docker

```
cp contrib/env_sample .env
docker-compose -f docker-compose.dev.yml up --build -d
```

Entre no container e crie um superuser:

```
docker container exec -it \
am-challenge_app_1 \
python manage.py createsuperuser \
--username="admin" --email=""
```

Criando alguns dados

```
docker container exec -it \
am-challenge_app_1 \
python manage.py create_data
```

## Tabelas

![mer.png](img/mer.png)

## Swagger

https://django-rest-swagger.readthedocs.io/en/latest/

url: `/doc/`

![swagger.png](img/swagger.png)

## Postman

Sem Docker a url base é `http://localhost:8000/`.

Com Docker é `http://0.0.0.0:8000/`.

http://localhost:8000/api/sale/sales/

`POST > JSON(application/json)`

```json
{
    "customer": 1,
    "seller": 1
}
```

`/api/sale/saleitems/`

```json
{
    "sale": 1,
    "product": 1,
    "quantity": 3,
    "price_sale": 1.5
}
```

> id dos produtos: 1 a 4.

## Relatórios

1. Para calcular a comissão total do vendedor

`/api/sale/sales/report_total_comission/`

e os parâmetros:

`start_date` = data no formato `yyyy-mm-dd`

`end_date` = data no formato `yyyy-mm-dd`

`seller_pk` = id do vendedor

Exemplo:

`/api/sale/sales/report_total_comission/?start_date=2020-08-23&end_date=2020-08-30&seller_pk=1`


2. Quais produtos e serviços um determinado cliente comprou num intervalo de tempo?

`/api/sale/sales/report_sales_product/`

e os parâmetros:

`start_date` = data no formato `yyyy-mm-dd`

`end_date` = data no formato `yyyy-mm-dd`

`customer_pk` = id do cliente


Exemplo:

`/api/sale/sales/report_sales_product/?start_date=2020-08-23&end_date=2020-08-30&customer_pk=1`


3. Produtos mais vendidos num intervalo de tempo.

`/api/sale/sales/report_top_products/`

e os parâmetros:

`start_date` = data no formato `yyyy-mm-dd`

`end_date` = data no formato `yyyy-mm-dd`

Exemplo:

`/api/sale/sales/report_top_products/?start_date=2020-08-23&end_date=2020-08-30`

## Endpoints

| endpoint                                     | Método                  | Ação                                                 |
|----------------------------------------------|-------------------------|------------------------------------------------------|
| /api/crm/customers/                          | GET, POST               | Retorna a lista de clientes ou insere um novo.       |
| /api/crm/customers/{pk}/                     | GET, PUT, PATCH, DELETE | Retorna o cliente com pk específico.                 |
| /api/crm/sellers/                            | GET, POST               | Retorna a lista de vendedores ou insere um novo.     |
| /api/crm/sellers/{pk}/                       | GET, PUT, PATCH, DELETE | Retorna o vendedor com pk específico.                |
| /api/product/products/                       | GET, POST               | Retorna a lista de produtos ou insere um novo.       |
| /api/product/products/{pk}/                  | GET, PUT, PATCH, DELETE | Retorna o produto com pk específico.                 |
| /api/sale/saleitems/                         | GET, POST               | Retorna a lista de itens da venda ou insere um novo. |
| /api/sale/saleitems/{pk}/                    | GET, PUT, PATCH, DELETE | Retorna o item da venda com pk específico.           |
| /api/sale/saleitems/add/                     | POST                    | Insere todos os produtos nos itens da compra.        |
| /api/sale/sales/                             | GET, POST               | Retorna a lista de vendas ou insere um novo.         |
| /api/sale/sales/{pk}/                        | GET, PUT, PATCH, DELETE | Retorna o venda com pk específico.                   |
| /api/sale/sales/report_sales_product/        | GET                     | Retorna os produtos comprados pelo **cliente** num intervalo de tempo. |
| /api/sale/sales/report_sales_product_detail/ | GET                     | Retorna os produtos comprados pelo **cliente** imediatamente após finalizar a compra. |
| /api/sale/sales/report_top_products/         | GET                     | Retorna os produtos mais vendidos.                   |
| /api/sale/sales/report_total_comission/      | GET                     | Retorna a comissão de um **vendedor** específico num intervalo de tempo. |


Parâmetros adicionais dos endpoints.

```
/api/sale/sales/report_sales_product/?start_date=2020-08-25&end_date=2020-08-30&customer_pk=1
/api/sale/sales/report_top_products/?start_date=2020-08-25&end_date=2020-08-30
/api/sale/sales/report_total_comission/?start_date=2020-08-25&end_date=2020-08-30&seller_pk=1
```



## Cypress

Na pasta `frontend` digite

`$ ./node_modules/.bin/cypress open`


## Links

https://www.django-rest-framework.org/

http://www.cdrf.co/

https://vuejs.org/

https://github.com/trevoreyre/autocomplete

https://www.cypress.io/

https://django-rest-swagger.readthedocs.io/en/latest/

https://www.django-rest-framework.org/community/3.10-announcement/#continuing-to-use-coreapi

https://github.com/ankurk91/vue-toast-notification

https://sweetalert2.github.io/
