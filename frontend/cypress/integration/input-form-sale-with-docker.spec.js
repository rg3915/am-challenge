let title = [
  'apontador',
  'borracha',
  'caneta',
  'papel sulfite',
]

describe('Input form', () => {
  it('focuses input on load', () => {
    cy.visit('http://0.0.0.0')

    cy.wait(1000)
    cy.get('.btn-success').click()

    for (var i = 4; i >= 0; i--) {
      var rand_title = title[Math.floor(Math.random() * title.length)]
      var rand_value = Math.floor(Math.random() * 10)
      cy.get('input[role="combobox"]').type(rand_title)
      cy.get('input[role="combobox"]').click().type('{downarrow}{enter}')
      cy.get('#id_quantity').clear()
      cy.get('#id_quantity').type(rand_value)
      cy.get('#btnAddProduct').click()
    }
    cy.get('#id_seller').select('Regis')
    cy.get('#id_customer').select('Acme')
    cy.get('button[type="submit"]').click()
  })
})
