import Vue from 'vue';
import Router from 'vue-router';

import Home from './views/Home';
import Sale from './views/Sale';
import CartItems from './views/CartItems';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [{
    path: '/',
    component: Home,
  }, {
    path: '/sale',
    component: Sale,
  }, {
    path: '/cart-items/:pk',
    component: CartItems,
  }]
});