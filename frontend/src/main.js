import Vue from 'vue'
import App from './App.vue'
import router from './router'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import axios from 'axios';

// font-awesome
import { library } from '@fortawesome/fontawesome-svg-core'
import { faTrash } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faTrash)

Vue.component('font-awesome-icon', FontAwesomeIcon)

// defaults.baseURL
// axios.defaults.baseURL = 'http://localhost:8000/api';
const baseurl = window.location.hostname
const endpoint = baseurl.concat(':8000/api')
axios.defaults.baseURL = 'http://'.concat(endpoint)

Vue.config.productionTip = false

// BootstrapVue
Vue.use(BootstrapVue);

// Autocomplete
import Autocomplete from '@trevoreyre/autocomplete-vue'
import '@trevoreyre/autocomplete-vue/dist/style.css'

Vue.use(Autocomplete)

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
